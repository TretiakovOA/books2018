﻿using System;
using Models;
using Models.AbstractClasses;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BooksAndCourses.Console
{
    public class LibResourceConverter: JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(LibResource);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject jObject = JObject.Load(reader);
            if (FieldExists(jObject, "Pages", JTokenType.Integer))
                return jObject.ToObject<Book>(serializer);
            if (FieldExists(jObject, "Length", JTokenType.Integer))
                return jObject.ToObject<VideoCourse>(serializer);
            if (FieldExists(jObject, "MainLink", JTokenType.String))
                return jObject.ToObject<InternetResource>(serializer);
            return null;
        }

        protected static bool FieldExists(JObject jObject, string name, JTokenType type)
        {
            return jObject.TryGetValue(name, out JToken token) && token.Type == type;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
