﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace BooksAndCourses.Console
{
    
    class DelegatesAndLambdas
    {
        private delegate double OurHandler(double input);

        public static void Operate()
        {
            OurHandler handler = null;
            do
            {
                double number = new Random().Next(1000000) / 100000.0;

                if (number > 5) handler = x => Math.Sqrt(x);                
                else handler = x => x * x;                

                double result = handler.Invoke(number);
                WriteLine(result);
            } while (ReadLine() != "q");
        }
    }
}
