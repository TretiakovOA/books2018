﻿using Models;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using Newtonsoft.Json;
using static System.Console;
using System.Linq;
using System;

namespace BooksAndCourses.Console
{
    class Program
    {
        private delegate void OurOperationHandler(Library library, string fileName);

        private static event OurOperationHandler _ourEvent;

        static void Main(string[] args)
        {
            Library library = LibInitializer.InitLibrary();

            //SerializationBinary(library, "library");
            //SerializationXml(library, "library");
            //SerializationJson(library, "library");

            //InvokeDelegate(library);
            DelegatesAndLambdas.Operate();

            ReadKey();
        }

        private static void InvokeDelegate(Library library)
        {
            _ourEvent= null;

            WriteLine("Выберите тип сериализации: 1. Бинарная. 2. XML. 3. JSON. 4. Все");
            string sInput = ReadLine();
            if (int.TryParse(sInput, out int iInput))
            {
                switch (iInput)
                {
                    case 1:
                        _ourEvent = SerializationBinary;
                        break;
                    case 2:
                        _ourEvent = SerializationXml;
                        break;
                    case 3:
                        _ourEvent = SerializationJson;
                        break;
                    case 4:
                        _ourEvent = SerializationBinary;
                        _ourEvent += SerializationXml;
                        _ourEvent += SerializationJson;
                        break;
                    default:
                        WriteLine("Неверное число");
                        break;
                }
            }
            else WriteLine("Введите число от 1 до 3");

            _ourEvent.Invoke(library, "library");
        }

        private static void SerializationBinary(Library library, string fileName)
        {
            fileName += ".bin";
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            using (FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate))
            {
                binaryFormatter.Serialize(fs, library);
            }
            using (FileStream fs = new FileStream(fileName, FileMode.Open))
            {
                Library newLibrary = (Library)binaryFormatter.Deserialize(fs);
            }
        }

        private static void SerializationXml(Library library, string fileName)
        {
            fileName += ".xml";
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Library));
            using (FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate))
            {
                xmlSerializer.Serialize(fs, library);
            }
            using (FileStream fs = new FileStream(fileName, FileMode.Open))
            {
                Library newLibrary = (Library)xmlSerializer.Deserialize(fs);
            }
        }

        private static void SerializationJson(Library library, string fileName)
        {
            fileName += ".json";
            JsonSerializer jsonSerializer = new JsonSerializer();
            jsonSerializer.Converters.Add(new LibResourceConverter());
            using (StreamWriter sw = new StreamWriter(fileName))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                jsonSerializer.Serialize(writer, library);
            }
            using (StreamReader sr = File.OpenText(fileName))
            {
                Library newLibrary = (Library)jsonSerializer.Deserialize(sr, typeof(Library));
            }
        }

    }
}
